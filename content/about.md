---
title: About me
date: 2017-11-08T16:56:15+02:00
draft: false
description: I am an Italian world nomad living, eating and sleeping in London (for the time being!).
header:
text_groups:
  - name: About
    description: I am an Italian world nomad living, eating and sleeping in London (for the time being!). When not designing, I’ll be in search of <b>sun, green spaces and good playgrounds</b> for my two kids to let off steam. Swimming in the crystal clear water of the sea is what makes me happy. I love travelling, meeting new people, and discovering different cultures. I secretly dream of repeating the round-the-world-trip I did ten years ago... but with my young family instead.
  - name: Experience
    description: <p><span class="grey-text">2022 – present</span></p><p><span class="default-text bold-text">NHS Digital</span> <p>Senior Service Designer</p></br><p><span class="grey-text">2020 – 2022</span></p><p><span class="default-text bold-text">NHS Digital</span> <p>Interaction Designer</p></br><p><span class="grey-text">2018 – 2020</span></p><p><span class="default-text bold-text">Home Office</span> <p>Interaction Designer</p></br><p><span class="grey-text">2015 – 2018</p></span><p><span class="default-text bold-text">Tempo</span> <p>Designer</p></br><span class="grey-text">2011 – 2015</span><p><span class="default-text bold-text">Mountview Academy of Theatre Arts, London, UK</span></p><p>Design & Website Manager</p></br><p><span class="grey-text">2009</p><span class="default-text bold-text">NGO GeSCI (UN ICT Task Force), Dublin, Ireland</span></p><p>Design & Communication Assistant</p></br><p><span class="grey-text">2007 - 2009</span></p><span class="default-text bold-text">DTA Architects, Dublin, Ireland</span></p><p>Graphic & Media Designer</p></br><span class="grey-text">2007</span><p><span class="default-text bold-text">Mather Hospital, Dublin, Ireland</span></p><p>Designer</p>
    class: line
  - name: Education
    description: <p><span class="grey-text">2003 - 2005</p><span class="default-text bold-text">MA in Industrial Design</span></p><p>Faculty of Design, Milan Polytechnic, Milan, Italy</p></br><p><span class="grey-text">2002 - 2003</span></p><span class="default-text bold-text">Communication Design</span></p><p>Institute of Arts, Orleans, France</p></br><p><span class="grey-text">1999 - 2002</span></p><span class="default-text bold-text">BA in Industrial Design</span></p><p>Faculty of Design, Milan Polytechnic, Milan, Italy</p>
    class: line
  - name: Courses
    description: <p><span class="grey-text">Present</span></p><p><span class="default-text bold-text">Interaction Design for Usability</span> <p><a href="https://www.interaction-design.org">The Interaction Design Foundation</a></p></br><p><span class="grey-text">Present</span></p><p><span class="default-text bold-text">Digital Accessibility</span> <p><a href="https://www.futurelearn.com/courses/digital-accessibility">Futurelearn</a></p></br><p><span class="grey-text">2008 - 2009</p></span><p><span class="default-text bold-text">Advanced Diploma in Web Design</span> <p>Dublin Business School, Dublin, Ireland</p></br><span class="grey-text">2006</span><p><span class="default-text bold-text">Graphics for Print Media</span></p><p>FÁS Training Centre, Dublin, Ireland</p>
    class: line


---
