---
title: allocating tasks
date: 2017-11-08T16:56:15+02:00
draft: false
description: Allocating tasks project, a piece of prioritisation software.


project_title:
  - name: Allocating tasks
image_1:
  - image:
      url: img/RP_1130x590.png
      alt_text: Photo of me during the workshop
      responsive_sources:
          "848": img/RP_848x443.png
          "565": img/RP_565x420.png
          "360": img/RP_360x318.png
text_groups:
  - name: Overview
    description: This project was a piece of prioritisation software used by staff to manage and allocate casework. The process of coordinating casework for managers and teams was extremely complex. The way to identify and prioritise relevant cases was fraught with legal considerations and complex business rules. Overall the process had become too time-consuming, cumbersome and expensive.
  - name: The challenge
    description: The system housed data on thousands of cases across the UK with individuals in varying circumstances. Some of these cases required simple engagement others physical interventions. The team needed software that could help them prioritise, and hence decide what level of intervention was needed. </br> </br>The users were dispersed on different teams all with different ways of working and workarounds to make the old system work. This often included large, unordered spreadsheets that were managed locally and were not controlled by a central function.
  - name: Process
    description: In a consulting role with the product owner and service manager I collaborated with them on the component that controlled how managers sort, distribute and delegate casework to teams. To understand what each actor in the system needed to do I hosted a workshop gathering knowledge and requirements from the product owner and business analyst.</br></br>This was a complex set of interactions to understand as there were many user groups. There two main active groups, team managers and caseworkers. The complexity lay in the allocation of work overlapping between different groups and teams.</br></br>After observing usability sessions and taking extensive notes I guided the team through affinity sorting to collate the main themes and issues. These were later placed in the context of a journey map. It became clear we needed to go one context up and create a process map of inherent hierarchies and overlapping remits of the system.
image_2:
    - image:
        url: img/rp_flow_1130x590.jpg
        alt_text: Flow diagram of the process mapping
        responsive_sources:
            "848": img/rp_flow_848x443.jpg
            "565": img/rp_flow_565x420.jpg
            "360": img/rp_flow_360x318.jpg
text_groups_2:
  - name: Solution
    description: I developed a process map based on the post-its and team notes on our whiteboard. With this the team understood the complexity and position of the managers within the system. New business rules and departmental objectives can change so managers needed to be able to task teams with the right work to meet those objectives. Filtering, allocating and dealing with refused or incomplete delegated tasks were activities they depended on.</br></br>With the process map as a basis I redesigned the manager view. The features and interactions of this UI were all related back to the requirements made plain from the process map.</br></br>The final wireframe shows the features including ways of making it easier for manager to search/filter cases and then allocate to teams or groups. </br></br>Designs of this work unfortunately cannot be shared here publicly but available on request. 
---
