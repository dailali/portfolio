---
title: filtering cases
date: 2017-11-08T16:56:15+02:00
draft: false
description: Filtering cases to review it in order to make a decision.

project_title:
  - name: Filtering cases
image_2:
  - image:
      url: img/st_testing_1130x590.jpg
      alt_text: Photo of me during the usability session
      responsive_sources:
            "848": img/st_testing_848x443.jpg
            "565": img/st_testing_565x420.jpg
            "360": img/st_testing_360x318.jpg
text_groups:
  - name: Overview
    description: This project was one of an application and adaptation of a framework inside a large enterprise-wide software. The team was trying to identify and prioritise cases to make a decision if a special surveillance technology was needed.
  - name: Objective
    description: I started the project with briefing sessions with the product owner and service manager to understand their ultimate intention and ideas. From this a working hypothesis was distilled forging a clear idea of what the feature needed to achieve. These ideas often explained as sketches were the starting point for a first draft solution which I then iterated on before playing back with low fidelity wireframes to the team.
text_groups_2:
  - name: Solution
    description: I designed an interface offering searching and filtering options to staff. The variation and relevance of filtering options gave users the ability to accurately and flexibly get to the right cases suitable for the surveillance technology. The first round of the usability testing showed the new design gave great flexibility to users to prioritise the right cases.</br></br>What was previously a fraught and difficult exercise was now flexible, efficient and easy-to-use, saving staff time and resources. Designs of this work unfortunately cannot be shared here publicly but available on request. 
---
