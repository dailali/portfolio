---
title: tempo website
date: 2017-11-08T16:56:15+02:00
draft: false
description: A new website for the former Spice Innovation Charity organisation.

project_title:
  - name: Tempo website
text_groups:
  - name: Overview
    description: In 2018 Spice (now called Tempo) a charity working in the voluntary sector went through a company rebrand. In collaboration with the communications team I was asked to redesign the website. Senior staff had noticed the poor performance of the site and wanted to improve the user experience. The CEO wanted to highlight the impact of the organisation. Encouraging ways for people to get involved was highlighted as a focus for business development.
  - name: The Problem
    description: The original website was created as an offshoot of some branding work with no user-centred design. Feedback showed users were finding it difficult to get the information they needed. There were large amounts of repetitive content and lots of extraneous information.
  - name: Research
    description: Talking to users we knew the majority coming to the site wanted to see how to spend their time credits. They needed to read the “spend brochures” but these were hidden within the site.<br><br>Looking at analytics we could see that only 20 of site visits were getting to the Spending Time Credits section. A new redesign would have to address the navigation and site structure to make this content more discoverable.<br><br> We could also see no users were reading blogs or case studies. The lack of newsletter sign-up was a business priority to improve so we researched what users thought.
text_groups_2:
  - name: Solution
    description: <p>After analysing our research and data I focused on three areas for improvement.</p><ol><li>Reorganised the site information architecture to match the users mental model of how they expected</li><li>Tidied up broken cumbersome and unclear filtering options and tidied page clutter allowing easier navigation</li><li>Highlighted and promoted a more upfront newsletter sign-up</li></ol> A lot of the website was difficult to read and scan because of the over-use of a heavy font in all caps. I moved the design away from an outdated icon heavy style using much fresher photographic style. The multi-colour branding was also too distracting so in collaboration with the branding agency we reduced to a more professional colour palette.
image_2:
  - image:
        url: img/tempo_flow_1130x590.jpg
        alt_text: Photo of the journey map of the new website
        responsive_sources:
          "848": img/tempo_flow_848x443.jpg
          "565": img/tempo_flow_565x420.jpg
          "360": img/tempo_flow_360x318.jpg
---
