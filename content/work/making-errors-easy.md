---
title: making errors easy
date: 2017-11-08T16:56:15+02:00
draft: false
description: Making the validation errors flow easy for the users.

project_title:
  - name: Making errors easy
text_groups:
  - name: Overview
    description: The purpose of this project was to look at how and if specialist facial recognition software could be used to find and locate individuals. if feasible this would be a huge time saver for security organisations. The service involved security staff uploading an image of a person and then accompanying video footage. After a period of data-crunching possibly lasting hours or days the service had to return matches.</br></br>I joined this team when they already had an MVP running with images and data already being tested with a large scale third party cloud platform. Making a usable and smooth experience and interacting with this massive computing power was part of the challenge we were trying to solve. They had also completed their first round of user testing.
  - name: The Challenge
    description: When reviewing the workflow of how users uploaded images and video we spotted a jarring experience when validating your uploads. Breaking Jacob’s fifth heuristic the service trapped users telling them they’d uploaded an erroneous file but didn’t give them any help to stop it.</br></br>The issue occurred because users uploading images went through rounds of validation. A client-side validation checking file format, file size, name etc and then a second validation with the external third-party cloud computing software. If a validation issue was spotted on the first step it would pass on to the second step throwing up an error for the second time. See video below.
image_2:
  - image:
      url: img/OldValidation_new.gif
      alt_text: Video playing the user journey during the upload files
      responsive_sources:
          "848": img/OldValidation_new.gif
          "565": img/OldValidation_new.gif
          "360": img/OldValidation_new.gif
text_groups_2:
  - name: Solution
    description: Working closely with the developer I mapped a new flow for the interaction and redesigned the interface. Our solution lay in putting all the validation actions at one point in the user journey.</br></br>With the user being told only once what has gone wrong it gives them time to recover and prevents them being stopped by the same error message again. There are the potential for lags from the passing of data between the service to the cloud platform. We’ll assess the extent to which this actually happens and if it affects users. The benefits of users being able to correctly understand what errors have been made is worth having even if they need to wait a few moments for it. Plus the removal of a potentially frustrating repetition for users.

---
