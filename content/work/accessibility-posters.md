---
title: filtering-cases
date: 2017-11-08T16:56:15+02:00
draft: false
description: The online site of the accessibility posters.

project_title:
  - name: Accessibility posters
text_groups:
  - name: Overview
    description: In 2016 the Home Office produced a series of posters explaining clearly the “do and don’t”s when creating accessible design. The poster have become a wild international success and have now been translated into 17 languages and become a go-to resource for the accessibility-minded around the globe. The posters really took off in the accessibility community with different designers forking the artwork and creating ones for a wide range of disabilities.
  - name: The brief
    description: Off the back of this success the team wanted to convert it from paper format to an online site. In digital format the recognition can spread even further to those using browsers and digital devices. Working in the open also fosters trust and collaborative working practices that enrich all design.
image_2:
  - image:
      url: img/ap_design_1130x590.jpg
      alt_text: Wireframes of the new Accessibility site
      responsive_sources:
        "848": img/ap_desing_848x443.jpg
        "565": img/ap_design_565x420.jpg
        "360": img/ap_design_360x318.jpg
text_groups_2:
  - name: Solution
    description: Starting by analysing the original artwork of that first poster I worked to reimagine what had now become an expanded collection with varying styles. First off was to create a landing page where people could discover the whole range of posters. The many hands involved lead the iconography to vary in consistency. I audited and reviewed the many variants and set out a new unified visual style making them more consistent. The content was also adapted to a web format with close attention to screen reader accessibility.
---
