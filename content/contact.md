---
title: Contact
date: 2017-11-09T14:50:58+02:00
draft: false
description: Feel free to get in touch with me via e-mail or any of my social media channels listed below.
header:
  description: Every project starts with a <span class="blue-text">conversation</span>, just drop me a line and let's create something great together.
  image:
    url: img/contact_img.png
    alt_text: The chair for meeting image
    responsive_sources:
      "848": img/contact_848x443.png
      "565": img/contact_565x420.png
      "360": img/contact_360x318.png
text_groups:
  - name: Get in touch
    description: <p>Feel free to get in touch with me via e-mail or any of my social media channels listed below.</p><br/><p>danila.lalli (a) gmail.com</p><p><a href="https://twitter.com/_dailali">twitter</a></p><p><a href= "https://www.linkedin.com/in/danila-lalli-87623054/">linkedIn</a></p>
    class: line
---
