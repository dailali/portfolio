---
title: Home page
date: 2017-11-01T18:28:28+02:00
draft: false
description: Hello - I’m Danila - an Italian born designer living and working in London. I enjoy creating simple and meaningful design experiences.
header:
  description: Hello, I’m Danila - an Italian born designer living and working in London. I enjoy creating simple and meaningful <span class="purple-text">design experiences.</span>
  image:
    url: img/home_img.png
    alt_text: The top of mountain image
    responsive_sources:
      "848": img/home_848x443.png
      "565": img/home_565x420.png
      "360": img/home_360x318.png
text_groups:
  - name: Philosophy
    description: I believe that <b>design is a never ending process.</b> Although it's very hard for a designer to accept it, the design of the final product is never perfect, it can always be improved. I enjoy working in multidisciplinary teams where experience and knowledge is shared through the different phases of the product's lifecycle. I am passionate about making things users really need and accessible to everyone.  
  - name: Get in touch
    description: <p>danila.lalli (a) gmail.com</p><p><a href="https://twitter.com/_dailali">twitter</a></p><p><a href= "https://www.linkedin.com/in/danila-lalli-87623054/">linkedIn</a></p>
    class: line
---
