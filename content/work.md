---
title: Home page
date: 2017-11-01T18:28:28+02:00
draft: false
text_groups:
  - name: Intro
    description: With my background being in industrial design and visual communication, I’m a conceptual thinker who naturally gravitates towards solving problems using creative solutions. I have designed for branding, print, online, incorporating User Experience (UX) and User interface (UI) design.
projects:
  - title: Allocating Tasks
    type: UI/UX/Service Design
    link: ../work/allocating-tasks
    image:
      url: img/rp_1130x590.jpg
      alt_text: Image of me while mapping the process on the wall
      responsive_sources:
        "848": img/rp_848x443.jpg
        "565": img/rp_565x420.jpg
        "360": img/rp_360x318.jpg
  - title: Filtering Cases
    type: UI/UX Design
    link: ../work/filtering-cases
    class: short-col
    image:
      url: img/st_n_364x590.jpg
      alt_text: Filtering cases
      responsive_sources:
        "848": img/st_n_848x443.jpg
        "565": img/st_n_565x420.jpg
        "360": img/st_n_360x318.jpg
  - title: Making Errors Easy
    type: UI/UX Design
    link: ../work/making-errors-easy
    class: wide-col
    image:
      url: img/ve_746x590.jpg
      alt_text: Wireframe of the new flow
      responsive_sources:
        "848": img/ve_848x443.jpg
        "565": img/ve_565x420.jpg
        "360": img/ve_360x318.jpg
  - title: Tempo Website
    type: web design
    link: ../work/tempo-website
    class: wide-col
    image:
      url: img/tempo_746x590.jpg
      alt_text: The photo of the new website
      responsive_sources:
        "848": img/tempo_848x443.jpg
        "565": img/tempo_565x420.jpg
        "360": img/tempo_360x318.jpg
  - title: Accessibility Posters
    type: UI/Graphic Design
    link: ../work/accessibility-posters
    class: short-col
    image:
      url: img/ap_364x590.jpg
      alt_text: The acccessibility posters
      responsive_sources:
        "848": img/ap_848x443.jpg
        "565": img/ap_565x420.jpg
        "360": img/ap_360x318.jpg
---
